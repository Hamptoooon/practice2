﻿namespace Practice2
{
    // Конкретная фабрика для создания сотрудников
    public class EmployeeFactory : IOrganizationComponentFactory
    {
        public IOrganizationComponent CreateComponent(string name)
        {
            return new Employee(name);
        }
    }
}
