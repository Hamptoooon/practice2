﻿namespace Practice2
{
    // Интерфейс компонента организации
    public interface IOrganizationComponent
    {
        string GetName();
        void Display(int depth);
    }
}
