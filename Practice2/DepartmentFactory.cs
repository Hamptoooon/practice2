﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice2
{
    // Конкретная фабрика для создания подразделений
    public class DepartmentFactory : IOrganizationComponentFactory
    {
        public IOrganizationComponent CreateComponent(string name)
        {
            return new Department(name);
        }
    }
}
