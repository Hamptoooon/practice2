﻿
namespace Practice2
{
    public class Program
    {
        
        static void Main(string[] args)
        {
            // Создаем фабрики для создания подразделений и сотрудников
            IOrganizationComponentFactory employeeFactory = new EmployeeFactory();
            IOrganizationComponentFactory departmentFactory = new DepartmentFactory();

            // Создаем структуру организации
            OrganizationComposite organization = new OrganizationComposite("Организация");
            IOrganizationComponent itDepartment = departmentFactory.CreateComponent("IT Отдел");
            IOrganizationComponent hrDepartment = departmentFactory.CreateComponent("Отдел кадров");
            IOrganizationComponent marketingDepartment = departmentFactory.CreateComponent("Маркетинговый отдел");

            organization.Add(itDepartment);
            organization.Add(hrDepartment);
            organization.Add(marketingDepartment);  

            ((OrganizationComposite)itDepartment).Add(employeeFactory.CreateComponent("Иван Иванов"));
            ((OrganizationComposite)itDepartment).Add(employeeFactory.CreateComponent("Петр Иванов"));
            ((OrganizationComposite)itDepartment).Add(employeeFactory.CreateComponent("Федор Иванов"));
            ((OrganizationComposite)itDepartment).Add(employeeFactory.CreateComponent("Сергей Иванов"));

            ((OrganizationComposite)hrDepartment).Add(employeeFactory.CreateComponent("Иван Иванов"));
            ((OrganizationComposite)hrDepartment).Add(employeeFactory.CreateComponent("Петр Иванов"));
            ((OrganizationComposite)hrDepartment).Add(employeeFactory.CreateComponent("Федор Иванов"));
            ((OrganizationComposite)hrDepartment).Add(employeeFactory.CreateComponent("Сергей Иванов"));

            ((OrganizationComposite)marketingDepartment).Add(employeeFactory.CreateComponent("Иван Иванов"));
            ((OrganizationComposite)marketingDepartment).Add(employeeFactory.CreateComponent("Петр Иванов"));
            ((OrganizationComposite)marketingDepartment).Add(employeeFactory.CreateComponent("Федор Иванов"));
            ((OrganizationComposite)marketingDepartment).Add(employeeFactory.CreateComponent("Сергей Иванов"));


            // Выводим структуру организации
            organization.Display(0);




        }
    }
}
