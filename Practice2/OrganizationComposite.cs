﻿using System;
using System.Collections.Generic;

namespace Practice2
{
    // Компоновщик для организации
    public class OrganizationComposite : IOrganizationComponent
    {
        private List<IOrganizationComponent> _components = new List<IOrganizationComponent>();
        private string _name;

        public OrganizationComposite(string name)
        {
            _name = name;
        }

        public string GetName()
        {
            return _name;
        }

        public void Add(IOrganizationComponent component)
        {
            _components.Add(component); 
        }

        public void Remove(IOrganizationComponent component)
        {
            _components.Remove(component);
        }

        public void Display(int depth)
        {       
            Console.WriteLine(new string('-', depth) + _name);
            foreach (var component in _components)
            {
                component.Display(depth + 2);
            }
        }
    }
}
