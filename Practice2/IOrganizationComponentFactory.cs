﻿namespace Practice2
{
    // Фабричный метод для создания компонентов организации
    public interface IOrganizationComponentFactory
    {
        IOrganizationComponent CreateComponent(string name);
    }
}
