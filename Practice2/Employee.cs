﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice2
{
    // Класс сотрудника
    public class Employee : IOrganizationComponent
    {
        private string _name;

        public Employee(string name)
        {  
            _name = name;
        }
        public string GetName()
        {
            return _name;
        }

        public void Display(int depth)
        {
            Console.WriteLine(new string('-', depth) + _name);
        }
    }
}
